# Summary

[Introduction](./intro.md)

# Setting

- [The World](./setting/world/intro.md)
    - [Planets and Megacorps](./setting/world/planets_and_megacorps.md)
        - [Mega-Corporations](./setting/world/megacorps.md)
        - [Zaibatsus](./setting/world/zaibatsus.md)
        - [Planets](./setting/world/planets.md)
        - [Corps Combat]()
        - [Tech Disparity]()
        - [The Exchange]()
    - [Technology](./setting/technology.md)
    - [Economy](./setting/economy.md)
    - [Transportation]()
    - [Combat Vehicles]()
    - [Firearms]()
- [About You]()
    - [Your Company](./setting/your_company.md)

# Rules
- [Character Creation]()
    - [etc]()

---
[*WARNING*](./warning.md)
[Ideas](./ruleIdeas.md)
[Contributors](./contributors.md)
[License](./license.md)
