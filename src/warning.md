# Warnings and Conduct

## Part the first: Conduct

Most TTRPGs these days have some kind of "Code of Conduct" or "Content Warning" associated with them, generally in the third or fourth page of the introduction. I'm going to go ahead and get it out of the way *now*.

Don't be an asshole to each other out of game.

That's it. If you want your game to be some kind of anti-fascist LGBTQIA++ we-are-all-friends-through-the-power-of-Marx, that's up to you. If you want to be Right Wing Death Squads, that is your prerogative. If you want to use those little cards with triggering content, fine. If someone doesn't want to, leave them alone. 

I have nothing but contempt for those who would dictate to *me* how, why, and *whom* I play with. Thus, I would be a hypocrite should I force such a thing on others.

## Part the second: Rules

In most discussions of Role-Playing Games, there are a few common points of contention not covered by the previous section.

### 1. Session Zero
To many, a Session Zero is a method to create an environment for their game - some see it as a necessity, others as a hindrance. Like most aspects of what I will add to this particular section, you should collaborate with the gamemaster and all the other members of your group to reach a solution. It's not my job to dictate such things.

### 2. Warning Cards
Absolutely the purview of the gamemaster and the group. This is not my problem, nor should it be.

### 3. Houserules and Rule Failure
Houserules aren't something I would ever deny. If it makes your game better (and your group agrees) thee is no problem with houserules. 

**Rule Failure** On the other hand, is completely my fault. If you find that one or more of the rules in this RPG just *don't work*, are unbalanced, or create an extremely undesirable outcome, please create an issue report on the [GitLab Page](https://gitlab.com/ChemicalHalo/boarding-axe-ttrpg).

### 4. Setting and Tone
I am designing this game to have a dark, brutal, nigh merciless atmosphere. I want players to be tense every combat, to be worried about knives in the back and upper management failing them. If you want this to be a TTRPG of *Deep Rock Galactic*, I cannot stop you. It could even be fun. Just know it's not my intention.

## Part the third: A Warning
I take no responsibility for how you play this game. How can I? I'm some (allegedly) other person, likely to never meet you. I hope you find some use out of this system, but I only have so much mental and emotional bandwidth. I can't care about everything - hell, I sometimes have a hard time caring about what is *actually* important. This game is to be used on it's own, and not with my blessing. I hope you enjoy it for what it is, but it's likely going to be clumsy and slow (at least at first).

