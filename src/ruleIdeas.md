# Rule Ideas

```admonish info
This is a page of mostly spitballing. Please do not take anything in here *too* seriously. Anything implemented will be added in a separate document from this one.
```

## To Do

- [ ] Crystallize main setting
- [ ] build out system of actions and combat
- [ ] build out how characters and items are build and comprised.
- [ ] work out balance (prelim)
- [ ] active playtesting
- [ ] tighten ruleset and setting
- [ ] prepare for limited-run printing


## Ideas

1. Combat
	- Fast
	- Brutal

Probably akin to Dark Heresy, WFRP, Only War, etc.

2. Hacking
	- Slightly less Fast

3. Computer Use
	- Although people are more likely to understand *why* most computers do what they do, their ability to use them may be hampered by their simplicity and lack of power.

4. Social Skills
	- Emphasize backstabbing
	- diplomancers should absolutely be a thing.

5. Piloting
	- Hardsuits, ship-based people-movers, cars, planes, ships of all sizes.
	- Hardsuits and other humanoid mecha should have some special combination of combat rules.

6. Item Creation
	- Should be available as an addon system at a later dictate

7. Character Parts
	- Quirks (minor impact, character defining)
	- Talents (important, but few and far between)
	- Hindrances (ranging from temporary inconvenience to permanent disability)
	- Skills (Learned through experience - with a bonus system a la Delta Green)
	- Feats (Feats are earned through actions)
	
8. Random Creation
	- The funnel system and Darwinian creations systems should both be possible. Random character creation in batches should be supported, even if requiring a tool.

9. Stats
	- Melee
	- Ballistic
	- Charisma
	- Strength
	- Toughness
	- Will
	- Agility
	- Intelligence
	- Perception

## System

d100 seems the best fit thus far. It can be pushed to be quick and understandable.

The system used by *Gamma Wolves* I think can be used as a model for mech/vehicular combat.
Skill Ideas
    - Mech Pilot
    - Gunnery
    - Tech Use
    - Tech Repair

