# Mega-Corporations

Mega-Corporations (Megacorps) are similar to the kinds of companies you may be familiar with. They operate like many other companies, with a board of directors, CEOs, shareholders, etc. They jointly control a set of regulations and stock markets - which has lead to an uneasy balance of power. Their constant vying for dominance has lead to an era of unimaginable wealth for the top .00005%, and constant demand for corporate warriors.

Though mutual agreement, no Megacorps will engage in open warfare with each other. This has lead to the modern era of corporate proxy-wars, with shell companies hiring mercenaries to raid other shell-companies.

## Some Examples

[[_TOC_]]

1. #### Interplanetary Logistics Machines
	- Once a medium-size logistics corporation, they found themselves in the perfect position to control the flow of items throughout multiple sectors. Their intense expansion and meteoric rise to prominence has not dulled their hunger for growth, nor their cutthroat response to competition. Ruthless and quick to respond, it doesn't pay to be in their bad graces.
	- Products and Services:
		- Shipping
		- Logistical Control
		- Medium planetary ports and computers
		- Medium cargo ships
		- Automated loading/unloading Machines
		- Automated defenses
2. #### Costruttore Astronavale Bizzarini
	- Builder of the most luxurious yachts in the universe, coincidentally the *only* shipwright to make large luxury yachts. Their ship design is often thought of as "beautiful", "striking", and "unorthodox" by the reviewers in *Ships Monthly*. In another strange coincidence, *Costruttore Astronavale Bizzarini* owns 85% of all ship-centric media, reviewing firms, and drydocks. They are also thought to have an over-large role in the ratification of ship standards across the galaxy.
	- Products and Services:
		- Luxury Yachts, Shuttles, Etc
		- High-Design collaborations with other companies
		- Racing and luxury combat ships
		- Fashion Design
		- Architecture
		- Drydocks and Building Construction
		- Ship design regulations and methods
3. #### Mister Jones' Happy Funtime Corp
	- The creator of the most delicious food in the galaxy, the MJHFC is more than pleased to sell you *"Meat<sup>tm</sup>"* rations and *"Algae<sup>tm</sup>"*-shakes. Their utter control of the vast predominance of agri-worlds, plant-derived shipping materials, and other such industries has lead them to a near-perfect vertical monopoly. Their lack of logistical control leads to constant friction with *ILM*, which sometimes bleeds into outright blockading.
	- Products and Services
		- Marketing
		- Printed Material
		- Shipping Material
		- Foodstuffs, Rations, Condiments, etc
		- Any plant-based Product
		- Blockade Running
4. #### Trans-Galaxian Broadcasting Service
	- If you need to know something, watch something, be entertained, or otherwise *stimulated*, *TBS* has you covered. Growing from a public-alert system on a single planet to a huge sector-spanning empire of broadcasting, information, prostitution, and drugs.
	- Products and Services
		- Prostitution
		- News Broadcasting
		- Entertainment programming
		- Recreational Drugs
		- Information protection and retrieval
		- Network intrusion and protection
5. #### Koenig Pharmaceuticals
	- From headache medicine to full-body prosthetics, Koenig has the brainpower to find a solution! When famed computer designer Koenig and the Kodamatic Medical Supply Company merged 100 years ago, it was thought such disparate companies would flounder as a single entity. This discounted Koenig's brilliant production designers and Kodamatic's preeminent research departments - shocking the galaxy with the first complete remedy to the common cold. Although less outwardly predatory to other companies, Koenig believes in playing the long game, and their obsession with being multiple steps ahead breeds a feeling of mistrust and disquiet whenever they deal with other companies.
	- Products and Services
		- Large and small - scale computers
		- Implants and Prosthetics
		- Networking hardware
		- Pharmaceuticals
		- Medical Supplies
		- Electronic Supplies
		- Quantum Predictive Strategy
		- Financial Services

