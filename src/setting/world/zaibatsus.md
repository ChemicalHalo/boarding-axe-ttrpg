# Zaibatsus

Zaibatsus are often more oddball in their corporate structure and culture. Some are traded on the Megacorps stock market; others are wholly private. Some are extremely transparent, others are a black box. Many of their services overlap with established Megacorps, leading to conflict in many ways - sometimes even a *spiritual* war for products and services will cause a disruption at the highest level.

Zaibatsus may have accords with each other individually, but there is no real ruling council or large treaty. Each individual conflict will be resolved as itself, and will not necessarily be applicable to the next one.

## Some Examples

[[_TOC_]]

1. #### Hahn-Cola Negation Supply
	-  A large beverage and electronic component manufacturer, Hahn-Cola prefers to play on a feeling of continuity and corporate nationalism to maintain it's consumer base. Why try some fly-by-night company that's only been around for 100 years, when you can use a Hahn built capacitor, the same one your great-great-grandaddy used. Reliable, inexpensive, and familiar, Hahn-Cola's products are often used by those who have used the before, not always questioning *why*.
	- Products and Services
		- Soft Drinks
		- Various Alcoholic Products and Beverages
		- Fuel
		- Electronic components
2. #### Great Dragons' Cave
	- Many people who want to choose their ships and computers want something that is a winner, and Great Dragon is committed to becoming a winner. Their ships and high-speed computation devices are often more expensive than the nearest competitor, but appear to function in a more performant fashion. The *Cult of Speed* that has formed around Great Dragon is seen by executives to be a great boon, but are otherwise disconnected from it. It is thought that eventually, the *Cult of Speed* may break away from Great Dragon, and become its own entity.
	- Products and Services
		- Race Ships
		- Interceptors
		- Processing Units
		- Stimulants
		- Engines
		- System Tuning
		- Large-Scale Systems Simulation
3. #### Xen
	- Through a complex, organized method of meditation combined with specialty cybernetics, Xen believes that they can accurately predict the future of the Megacorps market, as well as accurately and precisely navigate ships across the galaxy without the use of massive navigational computers. This has spawned a quiet, contemplative religion of monks, often forgoing the material world to better predict the future. Any allegations of heavy psychedelics usage, brainwashing, terror attacks against their rivals, and massive cover-ups of crashed ships is just slander and defamation against a religion.
	- Products and Services
		- Wetware processors
		- Specialty cognitive Implants
		- Navigators
		- Market Prediction
		- Smuggling
		- Psychedelic Drugs
		- Mental Supressants
4. #### Council of the Bear
	- Tracing their history back for more than a thousand years, the *Council of the Bear* is a hegemony-like association of multiple assassination and mercenary service companies, each with their own culture. The council is broadly responsible for the assignment of jobs, accreditation of units, training, and enforcement of rules. Operating similar to a renaissance-era worker's guild, the Council attempts to keep the companies in their care from going trigger-happy, breaking contracts, or losing council members' money.
	- Products and Services
		- Weapons
		- Assassinations
		- Mercenaries
		- Military Ships
		- Police Training
		- Defence Force Training
		- Guerilla Training


Zaibatsus are often more oddball in their corporate structure and culture. Some are traded on the Megacorps stock market; others are wholly private. Some are extremely transparent, others are a black box. Many of their services overlap with established Megacorps, leading to conflict in many ways - sometimes even a *spiritual* war for products and services will cause a disruption at the highest level.

Zaibatsus may have accords with each other individually, but there is no real ruling council or large treaty. Each individual conflict will be resolved as itself, and will not necessarily be applicable to the next one.

## Some Examples

[[_TOC_]]

1. #### Hahn-Cola Negation Supply
	-  A large beverage and electronic component manufacturer, Hahn-Cola prefers to play on a feeling of continuity and corporate nationalism to maintain it's consumer base. Why try some fly-by-night company that's only been around for 100 years, when you can use a Hahn built capacitor, the same one your great-great-grandaddy used. Reliable, inexpensive, and familiar, Hahn-Cola's products are often used by those who have used the before, not always questioning *why*.
	- Products and Services
		- Soft Drinks
		- Various Alcoholic Products and Beverages
		- Fuel
		- Electronic components
2. #### Great Dragons' Cave
	- Many people who want to choose their ships and computers want something that is a winner, and Great Dragon is committed to becoming a winner. Their ships and high-speed computation devices are often more expensive than the nearest competitor, but appear to function in a more performant fashion. The *Cult of Speed* that has formed around Great Dragon is seen by executives to be a great boon, but are otherwise disconnected from it. It is thought that eventually, the *Cult of Speed* may break away from Great Dragon, and become its own entity.
	- Products and Services
		- Race Ships
		- Interceptors
		- Processing Units
		- Stimulants
		- Engines
		- System Tuning
		- Large-Scale Systems Simulation
3. #### Xen
	- Through a complex, organized method of meditation combined with specialty cybernetics, Xen believes that they can accurately predict the future of the Megacorps market, as well as accurately and precisely navigate ships across the galaxy without the use of massive navigational computers. This has spawned a quiet, contemplative religion of monks, often forgoing the material world to better predict the future. Any allegations of heavy psychedelics usage, brainwashing, terror attacks against their rivals, and massive cover-ups of crashed ships is just slander and defamation against a religion.
	- Products and Services
		- Wetware processors
		- Specialty cognitive Implants
		- Navigators
		- Market Prediction
		- Smuggling
		- Psychedelic Drugs
		- Mental Supressants
4. #### Council of the Bear
	- Tracing their history back for more than a thousand years, the *Council of the Bear* is a hegemony-like association of multiple assassination and mercenary service companies, each with their own culture. The council is broadly responsible for the assignment of jobs, accreditation of units, training, and enforcement of rules. Operating similar to a renaissance-era worker's guild, the Council attempts to keep the companies in their care from going trigger-happy, breaking contracts, or losing council members' money.
	- Products and Services
		- Weapons
		- Assassinations
		- Mercenaries
		- Military Ships
		- Police Training
		- Defence Force Training
		- Guerilla Training


