# The World

*The SS Constellation's lights flicker on and off, alarms blaring. Fires pour from what vents are still supplying oxygen. The hull shudders as boarding Nails strike the weakest armor plates. Screams of the dying fade as the atmosphere vents into space. Warriors clad in enormous black powered armor move from the Nails, weapons in hand.*

*One way or another, the ship no longer belongs to the crew.*

```admonish warning
The worldbuilding is under constant and unpredictable development. Read and use at your own risk.
```


## Company Men
Mega-coporations and Zaibatsus control known space, governing every aspect from the media seen in every starport, the schedules of every super-hauler, the weather on agri-worlds, and the implants in your body. There is nothing that can escape their sight once they have reason to look.

Your world is your (rather smaller) mercenary company, specifically your platoon. You are only one of many on your ship, which is only one of many of your company, which is only one of many in your sector.

## Technology
The super-quantum-state computers of the Mega-Corporations and Zaibatsus can predict the future to an absurd degree, and utilize hacking techniques that one would assume to be magic. They have distinct personalities, and unerring decision making skills. To own one is to suddenly become a major player in the game of business and politics.

You don't get those.

Most technology used by anybody who isn't an Executive has the computing power of a calculator, and the viewscreen of a data terminal. Cathode-Ray tubes and 8088 processors rule the day, and only ship-based mainframes can have even a fraction the power the Mega-Corps have.
