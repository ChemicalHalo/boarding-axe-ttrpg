# Technology

Most of the Technology in the galaxy has undergone a massive upheaval. The drastic increase in the computer over that last few thousand years has largely been reverted. It was found that large-scale neural AI networks had infected huge swaths of the production spaces of the old empires. This led to the chip foundries to unwittingly produce hardware specifically for the growth of artificial intelligence. Once discovered - it was too late to recover most designs for integrated circuits. Many of the designs that the old empire relied on were destroyed, and only by the thinnest of margins was any of the old empire saved from the silicon parasite. 

Thus, technology had to take a slightly different path. The chips of old and the experimental, neither created at modern foundries, became the basis for all new computer, integrated circuit, and component design. Most technology that requires computation use what was once called "8088" or "64000" - or use "Quantum Entanglement Interfaces". Very little aside from these two poles of technological advancement exist.

Some more eccentric Zaibatsus who are attempting to use and upgrade the Vacuum Tube, but they have yet to rival Integrated Circuit powered technology.

Because of the newfound reliance on ancient technology, there have been enormous advances in the integrated circuit, cathode ray tube, and other "obsolete" technologies. The older processes have been continually refined and upgraded; but still rely on practically ancient technology.

Thus, the advancement of technology stagnates for most, with the fear of a new Parasite always influencing the decisions of the foundries.
