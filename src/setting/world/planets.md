# Planets

Most planets you may deal with will be under the loose jurisdiction of a major corporation. Those that are not fall into one of two camps:

 1. #### Tapped out Worlds
    - These worlds have almost nothing left to recommend them. No natural resources, little air and/or water, harsh environmental hazards, etc. There is nothing left here except some people. These people will often contract with *Council of the Bear* or other companies for training and protection from pirates or other invaders.
	
 2. #### The Frontier
    - These worlds may have little (known) resources to offer, but they do offer some semblance of (possibly temporary) freedom. No major Corps have laid claim, or forced their way in yet. It is usually only a matter of time before a large lode, cheap land, good farmland, or terraformable paradise entices a Megacorp or Zaibatsu to purchase or "acquire" the world.

Even when under control of a Megacorp or a Zaibatsu, most planets are left to their own devices when it comes to day-to-day governance and operation. One can find worlds of almost any description - from the most calm paradise worlds reserved for executive retreats, to slumworlds designed to house the billions needed for the orbital refineries.
