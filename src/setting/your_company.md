# Your Company

Ever since you signed up, you'll never be alone. Your implants feed you constant information about your company - Stock prices, your pay+bonuses, hours until retirement, your death bonus. You always know these things, and you know you are part of a larger whole. Your management don't seem to care much about *you*, but they do care about the job. You'll get enough slack to hang yourself - but not much more.

Most players will find themselves in the employ of a *Mercenary Company* - a company used for corporate warfare. Not always open combat, but espionage, price-fixing, extortion, etc, are all within the purview of a Mercenary Company. Most "Mercs" are organized, regulated, and given orders by the *Council of the Bear*. This includes when they are pitted against each other. No treaty lasts for long, and few cross-company friendships last long.

## Hierarchy

Most "Mercs" operate as a military do, but not all. Some operate more like a feudal lord, others more like a private investigation company. Yet others are organized like a mid 2010s silicon valley start-up. The *Council of the Bear* will not interfere in the daily operation of Mercs unless they fell a violation of their terms has taken place.

## Money

The most important part of any company's existence is the ability to perpetrate that existence. To that end, each company needs *Money*. The Council selects missions and pays the companies, minus a cut. The Council also will assist in the starting of a company, so long as that company pledges loyalty to the Council, and uses their loans to do so. It can take hundreds of years to pay off a council loan, thus some companies will break council law by taking third-party contracts. These contracts pay handsomely, but may come at the price of guild censure or audit.

## Materiel

Most Mercs get their ships, weapons, food, etc through contracts with the Council. This is not, however, a hard-and-fast rule, as many Mercs find the council to be unable to fulfill their immediate, local, or even dietary requirements. Most companies are allowed a certain degree of autonomy when it comes to procurement, but some are tied to a specific Megacorp. This will ultimately lessen their ability to get varied items, but may yield dividends in other ways. 
